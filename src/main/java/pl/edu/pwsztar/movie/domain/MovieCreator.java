package pl.edu.pwsztar.movie.domain;

import pl.edu.pwsztar.movie.dto.CreateMovieDto;

class MovieCreator {

    Movie from(CreateMovieDto createMovieDto){
        return Movie.builder()
                .videoId(createMovieDto.getVideoId())
                .title(createMovieDto.getTitle())
                .year(createMovieDto.getYear())
                .image(createMovieDto.getImage())
                .build();
    }

}
