package pl.edu.pwsztar.movie.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.pwsztar.movie.dto.*;


import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
public class MovieFacade {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieFacade.class);

    private final MovieRepository movieRepository;
    private final MovieCreator movieCreator;

    @Autowired
    public MovieFacade(MovieRepository movieRepository, MovieCreator movieCreator) {
        this.movieCreator=movieCreator;
        this.movieRepository = movieRepository;
    }

    public List<MovieDto> findAll() {
        return movieRepository
                .findAll().stream()
                .map(Movie::movieDto)
                .collect(Collectors.toList());
    }


    public void creatMovie(CreateMovieDto createMovieDto) {
        Movie movie = movieCreator.from(createMovieDto);
        movieRepository.save(movie);
    }


    public void deleteMovie(Long movieId) {
        movieRepository.deleteById(movieId);
    }


    public DetailsMovieDto findMovie(Long movieId) {
        Movie movie = movieRepository.findOneByMovieId(movieId);
        return movie.detailsMovieDto();
    }


    public MovieCounterDto countMovies() {
        return new MovieCounterDto.Builder().counter(movieRepository.count()).build();
    }


    public void updateMovie(Long movieId, UpdateMovieDto updateMovieDto) {
        Movie movie = movieRepository.findOneByMovieId(movieId);

        if (movie != null) {
            Movie movieBuilder = new Movie().builder()
                    .movieId(movieId)
                    .image(updateMovieDto.getImage())
                    .title(updateMovieDto.getTitle())
                    .videoId(updateMovieDto.getVideoId())
                    .year(updateMovieDto.getYear())
                    .build();
            movieRepository.save(movieBuilder);
        }
    }
}
