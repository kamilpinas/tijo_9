package pl.edu.pwsztar.movie.domain;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.edu.pwsztar.movie.dto.DetailsMovieDto;
import pl.edu.pwsztar.movie.dto.MovieDto;

@Builder
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "movies")
class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long movieId;
    private String title;
    private String image;
    private Integer year;
    private String videoId;

    MovieDto movieDto() {
        return MovieDto.builder()
                .movieId(this.movieId)
                .title(this.title)
                .image(this.image)
                .year(this.year)
                .build();
    }

    DetailsMovieDto detailsMovieDto() {
        return DetailsMovieDto.builder()
                .videoId(this.videoId)
                .title(this.title)
                .image(this.image)
                .year(this.year)
                .build();
    }
}

