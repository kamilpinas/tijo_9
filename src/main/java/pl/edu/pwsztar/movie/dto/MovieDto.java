package pl.edu.pwsztar.movie.dto;

import lombok.*;

@Builder(toBuilder = true)
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MovieDto{

    private Long movieId;
    private String title;
    private String image;
    private Integer year;

}

